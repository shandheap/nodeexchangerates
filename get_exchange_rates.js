// Load axios for handling HTTP requests
const axios = require('axios');
// Load moment for date parsing and manipulation
const moment = require('moment');
// Load readline to parse user input
const readline = require('readline');

// Create readline interface to stdin and stdout
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


/**
 * Function that gets rates from the exchange rates api and outputs them to stdout
 * @param  String  date  The specified date to get exchange rates for (eg. 2018/04/19)
 * @param  String  base_currency  The base currency for the exchange rates (eg. USD)
 * @param  Float   base_amount  The base amount of money that you want to convert (eg. 100)
 * @param  String  conversion_currency  The currency to convert to (eg. CAD)
 * @return Promise
 */
function getExchangeRates(date, base_currency, base_amount, conversion_currency) {
  // Construct the base api url by using the date and base currency
  const api_url = `https://exchangeratesapi.io/api/${date}?base=${base_currency}`;

  // Check if base_amount is not a valid number
  if (isNaN(base_amount)) {
    // Inform the user that the base amount entered is incorrect
    console.log("Error: Please enter a valid base amount to convert.\n");
    // Resolve the promise with undefined
    return Promise.resolve(undefined);
  }

  // Perform an axios GET request to the api
  return axios.get(api_url)
    // On success, parse the rates from the response json
    .then((res) => {
      const rates = res.data.rates;

      // Resolve the promise with the JSON response if the converted currency exists
      if (rates.hasOwnProperty(conversion_currency)) {
        let conversion_amount = (rates[conversion_currency] * base_amount);

        // Round the conversion amount to 4 decimal places
        conversion_amount = Number(conversion_amount.toFixed(4));

        return Promise.resolve({
          "date": date,
          "base_currency": base_currency,
          "base_amount": base_amount,
          "conversion_currency": conversion_currency,
          "conversion_amount": conversion_amount
        });
      }
      // Otherwise, tell user that the conversion currency is incorrect
      else {
        console.log("Error: Please enter a valid conversion currency.\n");
      }
    })
    // On error, show error message to user with valid example query
    .catch((err) => {
      console.log("Error: Please enter a valid query with the correct format for arguments.")
      console.log("Example: On April 10, 2018, how much was USD 100 in CAD?\n")
    });
}

/**
 * Function that parses user input from stdin and executes getExchangeRates if input is valid
 * @param  String
 * @return Promise
 */
function parseUserInput(input) {
  // Regular expression for user queries
  const reg_exp = /On (\w+) (\d+), (\d{4}), how much was (\w+) (\d*\.?\d*) in (\w+)\?/;

  // Match user input with regular expression
  const args = input.trim().match(reg_exp);

  // Declare return promise variable
  let promise;

  // There is a match with the regular expression so execute getExchangeRates
  if (args != null) {
    const date = moment(`${args[1]} ${args[2]} ${args[3]}`, "MMMM DD YYYY")
                  .format("YYYY-MM-DD");
    const base_currency = args[4];
    const base_amount = args[5];
    const conversion_currency = args[6];

    // Initialize return variable to async promise from getExchangeRates
    promise = getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      // Log the converted rate if it exists
      if (res != undefined && res != null) {
        console.log(res['conversion_amount']);
      }
    });
  }
  // Otherwise, tell user that the input is invalid
  else {
    console.log(
      "Error: Please enter a query containing the date, base currency, base amount and conversion currency.\n"
    );
    // Initialize return variable to empty promise
    promise = Promise.resolve();
  }

  return promise;
}

// Export getExchangeRates as default module
module.exports = getExchangeRates;

// Check if script was called from command line, and prompt the user if that's the case
if (require.main === module) {
  // Keep prompting user for input indefinitely, unless they type exit
  rl.setPrompt('Please enter an exchange rate query (type exit to end the program): ');
  rl.prompt();

  rl.on("line", (line) => {
    // Trim whitespace from user input
    const input = line.trim();

    // End the program if user types exit
    if (input === "exit") {
      rl.close();
    }

    else {
      // Parse user input and prompt after request has been completed
      parseUserInput(input)
        .then(() => rl.prompt())
        .catch((err) => console.log(err));
    }
  });
}