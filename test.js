const expect = require('chai').expect;
const getExchangeRates = require('./get_exchange_rates');

describe('getExchangeRates()', () => {
  it('should successfully get exchange rates from USD to CAD', () => {
    const date = "2011-06-03";
    const base_currency = "USD";
    const base_amount = 100;
    const conversion_currency = "CAD";
    const conversion_amount = 97.85;

    return getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      expect(res).to.deep.equal({
        "date": date,
        "base_currency": base_currency,
        "base_amount": base_amount,
        "conversion_currency": conversion_currency,
        "conversion_amount": conversion_amount
      });
    });
  });

  it('should successfully get exchange rates from GBP to SEK', () => {
    const date = "2007-07-12";
    const base_currency = "GBP";
    const base_amount = 303;
    const conversion_currency = "SEK";
    const conversion_amount = 4085.0157;

    return getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      expect(res).to.deep.equal({
        "date": date,
        "base_currency": base_currency,
        "base_amount": base_amount,
        "conversion_currency": conversion_currency,
        "conversion_amount": conversion_amount
      });
    });
  });


  it('should successfully get exchange rates from EUR to PLN', () => {
    const date = "2004-08-07";
    const base_currency = "EUR";
    const base_amount = 5;
    const conversion_currency = "PLN";
    const conversion_amount = 22.01;

    return getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      expect(res).to.deep.equal({
        "date": date,
        "base_currency": base_currency,
        "base_amount": base_amount,
        "conversion_currency": conversion_currency,
        "conversion_amount": conversion_amount
      });
    });
  });

  it('should successfully get exchange rates from ZAR to TRY', () => {
    const date = "2017-02-09";
    const base_currency = "ZAR";
    const base_amount = 132;
    const conversion_currency = "TRY";
    const conversion_amount = 36.3528;

    return getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      expect(res).to.deep.equal({
        "date": date,
        "base_currency": base_currency,
        "base_amount": base_amount,
        "conversion_currency": conversion_currency,
        "conversion_amount": conversion_amount
      });
    });
  });

  it('should fail to get exchange rates for incorrect date', () => {
    const date = "19/04/2017";
    const base_currency = "USD";
    const base_amount = 100;
    const conversion_currency = "CAD";

    return getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      expect(res).to.equal(undefined);
    });
  });

  it('should fail to get exchange rates for incorrect base currency', () => {
    const date = "2017-04-19";
    const base_currency = "FAKE";
    const base_amount = 100;
    const conversion_currency = "CAD";

    return getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      expect(res).to.equal(undefined);
    });
  });

  it('should fail to get exchange rates for incorrect base amount', () => {
    const date = "2017-04-19";
    const base_currency = "USD";
    const base_amount = "not_a_number";
    const conversion_currency = "CAD";

    return getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      expect(res).to.equal(undefined);
    });
  });

  it('should fail to get exchange rates for incorrect conversion currency', () => {
    const date = "2017-04-19";
    const base_currency = "USD";
    const base_amount = 100;
    const conversion_currency = "FAKE";

    return getExchangeRates(
      date,
      base_currency,
      base_amount,
      conversion_currency
    )
    .then((res) => {
      expect(res).to.equal(undefined);
    });
  });
});