# NodeExchangeRates

Install dependencies using `npm install`.

To run program, type `npm start` or `node get_exchange_rates`.

You can enter a query to get the converted rate. For example, type `On March 15, 2017, how much was USD 100 in EUR?`.

To run the tests, type `npm test`.